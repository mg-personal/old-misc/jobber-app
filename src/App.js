import React from "react";
// import firebase from './firebase'
// import { useForm } from "react-hook-form";
import { BrowserRouter as Router, Link, Route, Switch } from "react-router-dom";

import HomePage from "./views/HomePage";
import LoginPage from "./views/Authentication/LoginPage";
import SignupPage from "./views/Authentication/SignupPage";
import UserMenu from "./components/leftmenu";


import UserHeader from "./components/userheader";
import OverviewPage from "./views/OverviewPage";
import HirePage from ".//views/HirePage";

import { useState } from "react";


const Messages = () => {
  return <div>Messages</div>
}

const Settings = () => {
  return <div>Settings</div>;
};

const App = () => {
  const [auth, setAuth] = useState(false);

  if (auth === false) {
    return (
      <Router>
        <div className="grid grid-cols-4">
          <div className="col-span-1 w-full h-screen user-menu">
            <UserMenu/>
          </div>
          <div className="col-span-3 w-full h-screen user-view">
            <div className="border-b border-gray-400"><UserHeader/></div>
            <div>
              <Switch>
                <Route exact path="/" component={OverviewPage} />
                <Route path="/hiring" component={HirePage} />
                <Route path="/messages" component={Messages} />
                <Route path="/settings" component={Settings} />
              </Switch>
            </div>
          </div>
        </div>
      </Router>
    );
  } else {
    return (
      <Router>
        <div>
          <nav className="landing-bg-primary">
            <div className="flex justify-between">
              <p className="ml-5 text-5xl font-semibold">
                <Link to="/">Jobber</Link>
              </p>
              <div className="flex items-center mr-5">
                <button className="mr-2 landing-btn-primary font-medium px-6 py-1 rounded-md shadow-md">
                  <Link to="/login">Login</Link>
                </button>
                <button className="ml-2 landing-btn-secondary font-medium px-6 py-1 rounded-md shadow-md">
                  <Link to="/signup">Signup</Link>
                </button>
              </div>
            </div>
          </nav>

          <Switch>
            <Route exact path="/" component={HomePage} />
            <Route path="/login" component={LoginPage} />
            <Route path="/signup" component={SignupPage} />
          </Switch>
        </div>
      </Router>
    );
  }
};

export default App;
