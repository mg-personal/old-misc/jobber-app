import React from "react";

import profilePicture from "../assets/janko-ferlic-mIs_QHS1ht8-unsplash.jpg";
import arrowIcon from "../assets/cheveron-down.svg";

const HirePageUser = () => {
  return (
    <div className="flex flex-col hire-view-card rounded-lg mt-10 shadow-lg">
      <div className="flex items-center mx-5 my-5">
        <img
          className="h-16 w-16 mr-5 rounded-lg"
          alt="/"
          src={profilePicture}
        />
        <div className="ml-1">
          <p className="text-lg font-medium">Alex Spurun</p>
          <div className="flex justify-between">
            <div className="bg-gray-300 rounded-sm flex justify-center items-center">
              <p className="text-sm px-3 py-1 font-bold text-gray-500">$25</p>
            </div>
            <div className="flex justify-center items-center">
              <p className="text-sm px-3 py-1 font-bold text-gray-500">|</p>
            </div>
            <div className="">
              <p className="text-sm px-3 py-1 font-semibold text-gray-500">
                per hour
              </p>
            </div>
          </div>
        </div>
      </div>
      <div className="mx-5 my-2 flex justify-between items-center">
        <p className="text-sm font-medium">10 rating</p>
        <div>
          <div className="h-1 w-40 bg-green-300 rounded-full"></div>
        </div>
      </div>
      <div className="mx-5 my-6">
        <p className="text-base font-semibold">Roles</p>
        <div className="mt-4">
          <div className="grid grid-cols-3 gap-2">
            <div className="py-1 hover:shadow-lg hover:bg-gray-200 transition duration-300 transform hover:scale-105 border bg-gray-300 flex items-center justify-center rounded-sm">
              <p className="text-xs">Engineer</p>
            </div>
            <div className="py-1 hover:shadow-lg hover:bg-gray-200 transition duration-300 transform hover:scale-105 border bg-gray-300 flex items-center justify-center rounded-sm">
              <p className="text-xs ">Singer</p>
            </div>
            <div className="py-1 hover:shadow-lg hover:bg-gray-200 transition duration-300 transform hover:scale-105 border bg-gray-300 flex items-center justify-center rounded-sm">
              <p className="text-xs">Chef</p>
            </div>
            <div className="py-1 hover:shadow-lg hover:bg-gray-200 transition duration-300 transform hover:scale-105 border bg-gray-300 flex items-center justify-center rounded-sm">
              <p className="text-xs">Programmer</p>
            </div>
            <div className="py-1 hover:shadow-lg hover:bg-gray-200 transition duration-300 transform hover:scale-105 border bg-gray-300 flex items-center justify-center rounded-sm">
              <p className="text-xs">Gamer</p>
            </div>
            <div className="py-1 hover:shadow-lg hover:bg-gray-200 transition duration-300 transform hover:scale-105 border bg-gray-300 flex items-center justify-center rounded-sm">
              <p className="text-xs">Electricist</p>
            </div>
          </div>
        </div>
      </div>
      <div className="mx-5 my-6 flex flex-col">
        <div className="flex justify-between">
          <button className="hover:bg-blue-600 text-xs hover:text-white text-blue-600 font-semibold flex justify-center items-center border rounded-md py-2 px-5">
            BOOK NOW
          </button>
          <button className="hover:bg-blue-600 text-xs hover:text-white text-blue-600 font-semibold flex justify-center items-center border rounded-md py-2 px-10">
            SAVE
          </button>
        </div>
        <div className="mb-2">
          <button className="mt-2 w-full hover:bg-blue-600 text-xs hover:text-white text-blue-600 font-semibold flex justify-center items-center border rounded-md py-2 px-10">
            CHECK AVAILABILITY
          </button>
        </div>
      </div>
    </div>
  );
};

const HirePage = () => {
  return (
    <div className="mx-16 flex flex-col">
      <div className="flex justify-between items-center mt-10">
        <p className="font-semibold">18 results found</p>
        <div>
          <div className="flex hire-page-menu-item px-4 shadow-lg rounded-md">
            <div>
              <p className="font-semibold text-sm">Thursday 23 May</p>
              <p className="font-semibold text-xs text-gray-500">Date</p>
            </div>
            <div className="flex items-center justify-center ml-6">
              <img className="h-4 w-4" alt="/" src={arrowIcon} />
            </div>
          </div>
        </div>
      </div>

      <div className="grid grid-cols-3 gap-5">
        <HirePageUser />
        <HirePageUser />
        <HirePageUser />
      </div>
    </div>
  );
};

export default HirePage;
