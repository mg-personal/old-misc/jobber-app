import React from "react";

import guyProfileImg from "../assets/tim-from-inlytics-io-ZZAsbUp-7n8-unsplash.jpg";

const OverviewPagePaymentItem = props => {
  return (
    <div className="flex justify-between mx-10">
      <p className="text-xs font-thin mr-10">{props.serviceName}</p>
      <p className="text-xs font-semibold ml-10">{props.servicePrice}</p>
    </div>
  );
};

const OverviewPagePaymentSeparator = props => {
  return (
    <div className="flex justify-center items-center my-2 mx-6">
      <div className="user-view-payment-separator bg-gray-400 w-full"></div>
    </div>
  );
};

const OverviewPagePayment = props => {
  return (
    <div className="user-view-payment-box rounded-md shadow-lg">
      <p className="font-medium text-3xl mt-2 ml-4">Payments</p>
      <div className="mt-6 mb-6">
        <OverviewPagePaymentItem
          serviceName="Service 23/03/2020"
          servicePrice="$29.99"
        />
        <OverviewPagePaymentSeparator />
        <OverviewPagePaymentItem
          serviceName="Service 01/05/2020"
          servicePrice="$139.99"
        />
      </div>
    </div>
  );
};

const OverviewPageHireItem = props => {
  return (
    <div className="flex user-view-hire-item items-center rounded-lg transition duration-300 shadow-lg transform hover:scale-105 mt-4">
      <img
        className="h-10 w-10 rounded-full ml-4 my-4"
        alt="/"
        src={guyProfileImg}
      />
      <p className="font-semibold text-xs ml-5">Tim Stuart</p>
      <p className="font-semibold text-gray-500 text-xs ml-5 mr-5">
        10/03/2020 11:30hs
      </p>
    </div>
  );
};

const OverviewPageHires = props => {
  return (
    <div>
      <p className="font-medium text-3xl mt-2 mb-2">Hires</p>
      <OverviewPageHireItem />
      <OverviewPageHireItem />
      <OverviewPageHireItem />
    </div>
  );
};

const OverviewPage = () => {
  return (
    <div className="mx-16">
      <div>
        <p className="font-semibold text-5xl">Hello, Janko!</p>
      </div>
      <div className="flex justify-between mx-10 mt-20">
        <OverviewPageHires />
        <OverviewPagePayment />
      </div>
    </div>
  );
};

export default OverviewPage;
