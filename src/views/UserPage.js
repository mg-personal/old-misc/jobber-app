import React from "react";

import BaseGrid from "../components/basegrid";

const UserPage = () => {
  return (
    <div>
      <BaseGrid />
    </div>
  );
};

export default UserPage;
