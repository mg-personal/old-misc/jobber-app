const firebase = require("firebase");
// Required for side-effects
require("firebase/firestore");

firebase.initializeApp({
    // apiKey: 'AIzaSyBxPPYQpef5_TIOUMbvq_39t1BlDzMR0-U',
    // authDomain: 'jobber-app-c273a.firebaseapp.com',
    // projectId: 'jobber-app-c273a'
    apiKey: "AIzaSyBxPPYQpef5_TIOUMbvq_39t1BlDzMR0-U",
    authDomain: "jobber-app-c273a.firebaseapp.com",
    databaseURL: "https://jobber-app-c273a.firebaseio.com",
    projectId: "jobber-app-c273a",
    storageBucket: "jobber-app-c273a.appspot.com",
    messagingSenderId: "47500260477",
    appId: "1:47500260477:web:36e82136a656a235ed2d14"
});

export default firebase