import React, { useState, useEffect, useLayoutEffect } from "react";
import UserMenu from "./leftmenu";
import axios from "axios";

const BaseGrid = () => {
  const [fetchData, setFetchData] = useState();
  const [nameInput, setNameInput] = useState("");
  const [addressInput, setAddressInput] = useState("");
  const [userName, setUserName] = useState(
    localStorage.getItem("userName") || ""
  );

  // useEffect(() => {

  // });

  function updateFetchData() {
    axios
      .get("http://localhost:3030")
      .then(res => {
        setFetchData(
          res.data.map(item => {
            return <li key={Math.random()}>{item.name}</li>;
          })
        );
        // setFetchData(fetchData + res.data);
      })
      .catch(err => console.log(err));
  }

  function uploadToDB(event) {
    event.preventDefault();
    axios
      .post("http://localhost:3030/add-user", {
        key: Math.random(),
        name: nameInput,
        address: addressInput
      })
      .then(response => {
        console.log(response);
      })
      .catch(error => {
        console.log(error);
      });
  }

  function logoutFromDB(event) {
    event.preventDefault();
    console.log("LOGGED OUT");
    localStorage.removeItem("userName");
  }

  function testDB(event) {
    event.preventDefault();
    console.log("LOGGED IN");
    axios
      .post("http://localhost:3030/login", {
        username: "mauropgarcia96@gmail.com",
        password: "test"
      })
      .then(response => {
        console.log(response.data);
        localStorage.setItem("userName", response.data.user.firstName);
      })
      .catch(error => {
        console.log(error);
      });
  }

  function deleteFromDB(event) {
    event.preventDefault();

    axios
      .post("http://localhost:3030/delete-user", {
        name: nameInput,
        address: addressInput
      })
      .then(response => {
        console.log(response);
      })
      .catch(error => {
        console.log(error);
      });
  }

  return (
    <div className="grid grid-cols-4">
      <div className="col-span-1 w-full h-screen user-menu">
        {/* <UserMenu /> */}
      </div>
      <div className="col-span-3 w-full h-screen user-view">
        {/* <div>
          <div className="flex justify-center mb-10">
            <p className="font-bold text-2xl">postgreSQL Database</p>
            <p className="font-bold text-2xl">{userName}</p>
          </div>

          <form className="w-full">
            <div className="flex items-center">
              <div className="w-1/3">
                <label
                  className="block text-gray-500 font-bold"
                  htmlFor="username"
                >
                  Name
                </label>
              </div>
              <div className="w-2/3">
                <input
                  id="username"
                  type="text"
                  placeholder="enter a name"
                  onInput={e => setNameInput(e.target.value)}
                />
              </div>
            </div>
            <div className="flex items-center">
              <div className="w-1/3">
                <label
                  className="block text-gray-500 font-bold"
                  htmlFor="address"
                >
                  Address
                </label>
              </div>
              <div className="w-2/3">
                <input
                  id="address"
                  type="text"
                  placeholder="enter an address"
                  onInput={e => setAddressInput(e.target.value)}
                />
              </div>
            </div>
            <div className="flex items-center">
              <button
                className="bg-blue-100 hover:bg-blue-500"
                onClick={uploadToDB}
              >
                Upload to DB
              </button>
              <button
                className="text-gray-100 bg-red-400 hover:bg-red-800"
                onClick={deleteFromDB}
              >
                Delete from DB
              </button>
              <button
                className="ml-10 text-gray-100 bg-black hover:bg-red-800"
                onClick={testDB}
              >
                TEST
              </button>
              <button
                className="ml-10 text-gray-100 bg-black hover:bg-red-800"
                onClick={logoutFromDB}
              >
                LOG OUT
              </button>
            </div>
          </form>
          <button onClick={updateFetchData}>Refresh</button>
          <div className="mt-10">
            <ul>{fetchData}</ul>
          </div>
        </div> */}
      </div>
    </div>
  );
};

export default BaseGrid;
