import React from "react";
import logo from "../assets/bitrise-icon.svg";
import UserMenuButton from "./leftmenubutton";
import overviewlogo from "../assets/browser-window.svg";
import hirelogo from "../assets/location-restroom.svg";
import messageslogo from "../assets/envelope.svg";
import settingslogo from "../assets/cog.svg";
import logoutlogo from "../assets/close-outline.svg";

const UserMenu = () => {
  return (
    <div>
      <div className="flex justify-center mt-5">
        <img className="logo" alt="/" src={logo} />
      </div>
      <div className="mt-20">
        <UserMenuButton route="/" name={"Overview"} logo={overviewlogo} />
        <UserMenuButton route="/hiring" name={"Hire"} logo={hirelogo} />
        <UserMenuButton route="/messages" name={"Messages"} logo={messageslogo} />
        <UserMenuButton route="/settings" name={"Settings"} logo={settingslogo} />
      </div>
      <div className="mt-64">
        <UserMenuButton route="/logout" name={"Logout"} logo={logoutlogo} />
      </div>
    </div>
  );
};

export default UserMenu;
