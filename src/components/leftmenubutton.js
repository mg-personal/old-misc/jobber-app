import React from "react";

import {Link} from "react-router-dom";

const UserMenuButton = props => {
  return (
    <div className="mt-1">
      <div className="leftmenubutton-div py-2">
        {/* <a href="/" className=""> */}
        <Link to={props.route}>
          <div className="flex items-center justify-center">
            <div className="w-1/3 flex items-center bg-red">
              <img
                className="leftmenubutton-logo mr-2"
                alt="/"
                src={props.logo}
              />
              <p className="text-gray-600 text-base ml-2">{props.name}</p>
            </div>
          </div>
        </Link>
      </div>
    </div>
  );
};

export default UserMenuButton;
