import React from "react";

import firstLogo from "../assets/undraw_coding_6mjf.svg";
import secondLogo from "../assets/undraw_calendar_dutt.svg";
import thirdLogo from "../assets/undraw_swipe_profiles1_i6mr.svg";
import checkLogo from "../assets/checkmark.svg";

const LandingPage = () => {
  return (
    <div className="h-full">
      <div className="grid grid-rows-3 h-full">
        <div className="row-span-2 landing-bg-primary">
          <div className="flex justify-center items-center w-full h-full">
            <div className="w-1/2 h-full">
              <p className="font-bold text-5xl">Find the best</p>
              <p className="font-bold text-5xl">worker</p>
              <p className="font-bold text-5xl">for your needs.</p>
              <div className="flex flex-row items-center mt-5 ml-10">
                <img className="h-4 w-4" alt="/" src={checkLogo} />
                <p className="font-semibold ml-4">Transparent Pricing</p>
              </div>
              <div className="flex flex-row items-center mt-4 ml-10">
                <img className="h-4 w-4" alt="/" src={checkLogo} />
                <p className="font-semibold ml-4">Trustable</p>
              </div>
              <div className="flex flex-row items-center mt-4 ml-10">
                <img className="h-4 w-4" alt="/" src={checkLogo} />
                <p className="font-semibold ml-4">Dedicated Customer Service</p>
              </div>
            </div>
          </div>
        </div>
        <div className="row-span-1 landing-bg-secondary">
          <div className="flex justify-center mt-2">
            <div>
              <p className="font-semibold text-4xl">How it works</p>
            </div>
          </div>
          <div className="flex justify-center">
            <div className="mx-10">
              <div className="flex justify-center">
                <img className="landing-svg" alt="/" src={firstLogo} />
              </div>
              <div className="flex justify-center">
                <p className="font-semibold">1. Enter your job</p>
              </div>
            </div>
            <div className="mx-10">
              <div className="flex justify-center">
                <img className="landing-svg" alt="/" src={secondLogo} />
              </div>
              <div className="flex justify-center">
                <p className="font-semibold">2. Choose your preferred date</p>
              </div>
            </div>
            <div className="mx-10">
              <div className="flex justify-center">
                <img className="landing-svg" alt="/" src={thirdLogo} />
              </div>
              <div className="flex justify-center">
                <p className="font-semibold">3. Book your Jobber</p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default LandingPage;
