import React from "react";

import userImage from "../assets/janko-ferlic-mIs_QHS1ht8-unsplash.jpg";
import arrowDown from "../assets/cheveron-down.svg";

const UserHeaderBox = () => {
  return (
    <div className="flex items-center my-2 mr-16 border-0 user-view-header-box rounded-md shadow-md">
      <div>
        <img className="h-16 w-16 rounded-l-lg" alt="/" src={userImage} />
      </div>
      <div>
        <p className="font-medium ml-5">Janko Ferlic</p>
      </div>
      <div>
        <img className="h-5 w-5 mx-5" alt="/" src={arrowDown} />
      </div>
    </div>
  );
};

const UserHeader = () => {
  return (
    <div className="flex justify-end">
      <UserHeaderBox />
    </div>
  );
};

export default UserHeader;
