# Jobber

### [Check out the demo here] (www.notavailableyet.com)

## About ⭐️

Jobber is an application designed to **connect employers and employees**.
It's **easy to use** and designed to be simple and fast using **reusable components**.

## Technology 💡

- [MongoDB](https://www.mongodb.com)
- [Express.js](https://expressjs.com)
- [React](https://reactjs.org)
- [Node.js](https://nodejs.org/en/)
- [TalwindCSS](https://tailwindcss.com)
- [Rebass](https://rebassjs.org)
- [PostgreSQL](https://www.postgresql.org)
- Icons from [Zondicons](https://www.zondicons.com)
- Responsive Design
- Developer Tools
    - eslint
    - WebStorm
    - Terminal

## Screenshots 📷

#### Home
![home](https://i.imgur.com/1VLMsaz.png)

#### Overview
![overview](https://i.imgur.com/ti4G0Gv.png)

#### Hiring
![hiring](https://i.imgur.com/vXdixR8.png)

#### Messages
![messages](https://i.imgur.com/OYOr5Jv.png)

#### Settings
![settings](https://i.imgur.com/IXwyfYI.png)

## License ⚠️

[![License: MIT](https://img.shields.io/badge/License-MIT-yellow.svg)](https://opensource.org/licenses/MIT)